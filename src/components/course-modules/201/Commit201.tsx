import { Container, Divider, Box, Button, Spacer, Flex, Heading, Grid, GridItem, Text } from "@chakra-ui/react";
import Link from "next/link";
import React, { useContext, useState, useEffect } from "react";

import CommitLayout from "@/src/components/lms/Lesson/CommitLayout";
import CommitmentTx from "@/src/components/gpte/transactions/CommitmentTx";
import Commit from "@/src/components/course-modules/201/Commit.mdx";
import { PPBLContext } from "@/src/context/PPBLContext";
import { getInlineDatumForContributorReference } from "@/src/data/queries/getInlineDatumForContributorReference";
import { checkReferenceDatumForPrerequisite } from "@/src/utils";
import { useWallet } from "@meshsdk/react";

const Commit201 = () => {
  const { connected } = useWallet();
  const ppblContext = useContext(PPBLContext);
  const selectedProject = "Module201";

  const [refDatum, setRefDatum] = useState<any | undefined>(undefined);
  const [hasPrerequisite, setHasPrerequisite] = useState(false);

  // Next step:
  // Create a helper function that takes refDatum and prereq module(s) as input
  // And returns true if student has those

  useEffect(() => {
    const fetchDatum = async () => {
      if (ppblContext.connectedContribToken && ppblContext.connectedContribToken.length > 0) {
        const _datum = await getInlineDatumForContributorReference(ppblContext.connectedContribToken);
        setRefDatum(_datum);
      }
    };

    if (ppblContext.connectedContribToken) {
      fetchDatum();
    }
  }, [ppblContext.connectedContribToken]);

  useEffect(() => {
    if (refDatum) {
      setHasPrerequisite(
        checkReferenceDatumForPrerequisite(refDatum, [
          "4d6f64756c65313033206e6f204769744c6162",
          "4d6f64756c653130332077697468204769744c6162",
        ])
      );
    }
  }, [refDatum]);

  return (
    <CommitLayout moduleNumber={201}>
      <Grid templateColumns="repeat(5, 1fr)" templateRows="repeat(2, 1fr)" gap={5}>
        <GridItem colSpan={[5, 5, 5, 5, 5, 3]} rowSpan={2}>
          <Commit />
        </GridItem>
        <GridItem colSpan={[5, 5, 5, 5, 5, 2]} border="1px" borderColor="theme.yellow" borderRadius="md" p="3">
          {hasPrerequisite ? (
            <>
              <Box my="5">{ppblContext.treasuryUTxO && <CommitmentTx selectedProject={selectedProject} />}</Box>
            </>
          ) : (
            <>
              <Box my="5">
                <Text fontSize="lg" fontWeight="900" color="theme.yellow" pb="3">
                  You must complete a <Link href="/modules/103/commit">Module 103 commitment</Link> before you can
                  commit to Module 201.
                </Text>
                {ppblContext.connectedContribToken && (
                  <Text fontSize="sm" fontWeight="300" color="theme.light">
                    {ppblContext.connectedContribToken}
                  </Text>
                )}
                {!connected && (
                  <Text fontSize="sm" fontWeight="300" color="theme.light">
                    Connect a wallet to check completion status.
                  </Text>
                )}
              </Box>
            </>
          )}
        </GridItem>
      </Grid>
    </CommitLayout>
  );
};

export default Commit201;
